@extends('template')
@section('title', 'Home | Student Portal')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="jumbotron">
            <h2>
                Welcome
            </h2>
            <p>
                View all the students in our site.
            </p>
            <p>
                <a class="btn btn-primary btn-large" href="#">Learn more</a>
            </p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-4 col-md-4 text-center">
        <button class="btn btn-primary btn-lg">Login</button>
        <button class="btn btn-success btn-lg">Register</button>
    </div>
</div>

@endsection